# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from . import account_analytic_account
from . import account_bank_statement
from . import account_history_importation
from . import account_invoice
from . import account_journal
from . import account_payment
from . import account_reconciliation_widget
from . import amc_report_launcher_abstract
from . import acm_auto_reconciliation_rule
from . import acm_bilan
from . import acm_expense_evolution
from . import acm_profit_share
from . import bank_statement_import_rawbank_pos
from . import bank_statement_import_tmb_caisse
from . import bank_statement_import_tmb_pos
from . import budget
from . import ir_attachment
from . import res_partner
