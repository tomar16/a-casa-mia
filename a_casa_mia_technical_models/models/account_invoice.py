# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import models, fields, api


class AcmAccountInvoice(models.Model):
    _inherit = 'account.invoice'

    imported_from_file_id = fields.Many2one('acm.account.history.importation', string=u"Fichier d'import",
                                            readonly=True)
    payment_mode_free_string = fields.Char(string=u"Mode de paiement")

    @api.model
    def default_get(self, fields_list):
        result = super(AcmAccountInvoice, self).default_get(fields_list)
        if 'type' in result:
            if result['type'] in ['out_invoice', 'out_refund']:
                customer = self.env['res.partner'].search([('customer', '=', True)])
                if len(customer) == 1:
                    result['partner_id'] = customer.id
        if 'date_invoice' not in result:
            result['date_invoice'] = fields.Date.today()
        return result
