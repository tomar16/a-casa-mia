# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

from odoo import models, api

_logger = logging.getLogger(__name__)


class AcmBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    @api.multi
    def start_auto_reconciliation(self):
        for rec in self:
            rules = self.env['acm.auto.reconciliation.rule'].search([('journal_id', '=', rec.journal_id.id)])
            if not rules:
                return
            nb_lines = len(rec.line_ids)
            index = 0
            for statement_line in rec.line_ids:
                index += 1
                if statement_line.journal_entry_ids:
                    continue
                for rule in rules:
                    if rule.startswith and statement_line.name.startswith(rule.startswith):
                        _logger.info(u"Statement line '%s' (%s/%s) attached to account %s",
                                     statement_line.name, index, nb_lines, rule.account_id.display_name)
                        new_aml_dict = {
                            'name': statement_line.name,
                            'credit': statement_line.amount >= 0 and statement_line.amount or 0,
                            'debit': statement_line.amount < 0 and (-1) * statement_line.amount or 0,
                            'analytic_tag_ids': [[6, None, []]],
                            'account_id': rule.account_id.id,
                        }
                        if rule.analytic_account_id:
                            new_aml_dict['analytic_account_id'] = rule.analytic_account_id.id
                        new_aml_dicts = [new_aml_dict]
                        statement_line.process_reconciliation(counterpart_aml_dicts=[],
                                                              payment_aml_rec=self.env['account.move.line'],
                                                              new_aml_dicts=new_aml_dicts)

    @api.model
    def create(self, vals):
        result = super(AcmBankStatement, self).create(vals)
        result.start_auto_reconciliation()
        return result

    @api.multi
    def write(self, vals):
        result = super(AcmBankStatement, self).write(vals)
        if 'line_ids' in vals:
            self.start_auto_reconciliation()
        return result


class AcmBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    @api.model
    def create(self, vals):
        _logger.info(u"Creating statement line with data %s", vals)
        return super(AcmBankStatementLine, self).create(vals)
