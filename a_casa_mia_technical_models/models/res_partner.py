# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import models, fields, api, exceptions


class AcmResPartner(models.Model):
    _inherit = 'res.partner'

    def _get_default_income_analytic_account_id(self):
        return self.env.ref('fill_empty_analytic_accounts.analytic_account_sales').id

    def _get_default_expense_analytic_account_id(self):
        return self.env.ref('fill_empty_analytic_accounts.analytic_account_purchases').id

    income_analytic_account_id = fields.Many2one('account.analytic.account',
                                                 default=_get_default_income_analytic_account_id)
    expense_analytic_account_id = fields.Many2one('account.analytic.account',
                                                  default=_get_default_expense_analytic_account_id)

    @api.multi
    def unlink(self):
        if self.env.ref('a_casa_mia_config.a_casa_mia_unknown_customer') in self:
            raise exceptions.UserError(u"Il est impossible de supprimer le client inconnu, car il est utilisé dans le "
                                       u"mécanisme de l'import comptable.")
        return super(AcmResPartner, self).unlink()
