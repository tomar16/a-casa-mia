# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import models, fields, api


class AcmAccountJournal(models.Model):
    _inherit = 'account.journal'

    def _get_bank_statements_available_import_formats(self):
        """ Adds new items to supported import formats."""
        result = super(AcmAccountJournal, self)._get_bank_statements_available_import_formats()
        result.append('rawbank_pos')
        result.append('tmb_pos')
        return result

    @api.multi
    def open_action(self):
        result = super(AcmAccountJournal, self).open_action()
        if isinstance(result, dict) and result.get('view_mode', ''):
            context = result.get('context', {})
            context['search_default_invoicedate'] = True
            result['context'] = context
        return result
