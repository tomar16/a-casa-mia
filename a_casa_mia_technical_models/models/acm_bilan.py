# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import base64
import logging
from io import BytesIO

import xlsxwriter

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class AcmBilanLauncher(models.TransientModel):
    _name = 'acm.bilan.launcher'
    _inherit = ['quick.create.ir.attachment.abstract', 'acm.report.launcher.abstract']
    _description = u"Pop-up de lancement pour le bilan ACM"

    detail_per_month = fields.Boolean(u"Détailler par mois")
    compare_budget = fields.Boolean(string=u"Comparer avec le budget prévisionnel")

    @api.model
    def get_budget_data_dict(self, date_from, date_to):
        # TODO: proratiser
        self.env.cr.execute("""WITH dates AS (
  SELECT %s::DATE AS date_from,
         %s::DATE AS date_to)

SELECT
  line.analytic_account_id,
  sum(COALESCE(line.planned_amount, 0)) AS total_planned_amount
FROM crossovered_budget_lines line
WHERE NOT (line.date_from < (SELECT date_from FROM dates) AND line.date_to < (SELECT date_from FROM dates))
  AND NOT (line.date_from > (SELECT date_to FROM dates) AND line.date_to > (SELECT date_from FROM dates))
GROUP BY line.analytic_account_id""", (date_from, date_to,))
        return {self.env['account.analytic.account'].browse(item[0]): item[1]
                for item in self.env.cr.fetchall()}

    @api.multi
    def generate_frame_bilan_worksheet(self, worksheet, style_title_blue_background_full_border,
                                       style_title_orange_background, style_title_orange_background_centered,
                                       style_simple_text, style_title_blue_background_centered):
        self.ensure_one()
        date_from, date_to = self.compute_dates_from_to()
        revenue_dict, cost_of_sales_dict, net_overhead_costs_dict = self. \
            get_analytic_revenue_subtotals_for_period(date_from, date_to)
        revenue_accounts = list(revenue_dict.keys())
        cost_of_sales_accounts = list(cost_of_sales_dict.keys())
        net_overhead_costs_accounts = list(net_overhead_costs_dict.keys())

        if self.compare_budget:
            budget_data_dict = self.get_budget_data_dict(date_from, date_to)
            for analytic_account in budget_data_dict:
                if analytic_account.bilan_type == 'revenue' and analytic_account not in revenue_accounts:
                    revenue_accounts += [analytic_account]
                if analytic_account.bilan_type == 'cost_of_sales' and analytic_account not in cost_of_sales_accounts:
                    cost_of_sales_accounts += [analytic_account]
                if analytic_account.bilan_type == 'net_overhead_costs' and \
                        analytic_account not in net_overhead_costs_accounts:
                    net_overhead_costs_accounts += [analytic_account]
        column_num = 0
        line_num = 1
        if self.compare_budget:
            line_num += 1
        worksheet.write(line_num, column_num, u"Revenue", style_title_blue_background_full_border)
        worksheet.write(line_num - 1, column_num + 1, u"Note", style_title_blue_background_centered)
        worksheet.write(line_num, column_num + 1, u"1", style_title_blue_background_centered)
        line_num += 1
        for account in revenue_accounts:
            worksheet.write(line_num, column_num, account.name, style_simple_text)
            line_num += 1
        line_num += 2
        worksheet.write(line_num, column_num, u"Cost of sales", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"2", style_title_blue_background_centered)
        line_num += 1
        for account in cost_of_sales_accounts:
            worksheet.write(line_num, column_num, account.name, style_simple_text)
            line_num += 1
        worksheet.write(line_num, column_num, u"Profitability coefficient", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"", style_title_blue_background_centered)
        line_num += 3
        worksheet.write(line_num, column_num, u"Gross profit", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"3", style_title_blue_background_centered)
        line_num += 3
        worksheet.write(line_num, column_num, u"Profit/(loss) before operating expenses",
                        style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"4", style_title_blue_background_centered)
        line_num += 3
        worksheet.write(line_num, column_num, u"Net overhead costs", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"5", style_title_blue_background_centered)
        line_num += 1
        for account in net_overhead_costs_accounts:
            worksheet.write(line_num, column_num, account.name, style_simple_text)
            line_num += 1
        line_num += 2
        worksheet.write(line_num, column_num, u"EBITDA", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"6", style_title_blue_background_centered)
        line_num += 2
        worksheet.write(line_num, column_num, u"Net Finance Cost", style_simple_text)
        line_num += 3
        worksheet.write(line_num, column_num, u"Operating profit/(loss)", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"7", style_title_blue_background_centered)
        line_num += 2
        worksheet.write(line_num, column_num, u"TVA réelle", style_simple_text)
        line_num += 1
        worksheet.write(line_num, column_num, u"TVA réconciliée", style_simple_text)
        line_num += 3
        worksheet.write(line_num, column_num, u"Profit/(loss)", style_title_blue_background_full_border)
        worksheet.write(line_num, column_num + 1, u"8", style_title_blue_background_centered)
        profit_shares = self.env['acm.profit.share'].search([])
        if profit_shares:
            line_num += 1
            for profit_share in profit_shares:
                line_num += 1
                profit_share_display = (u"Profit Share %s (%s" % (profit_share.name, profit_share.percentage)) + u"%)"
                worksheet.write(line_num, column_num, profit_share_display, style_simple_text)
                worksheet.write(line_num, column_num + 1, u"", style_simple_text)
        line_num += 3
        worksheet.write(line_num, column_num, u"Profit/(loss)", style_title_orange_background)
        worksheet.write(line_num, column_num + 1, u"9", style_title_orange_background_centered)
        # Élargissement de la colonne
        worksheet.set_column(column_num, column_num, 45, None)
        return revenue_accounts, cost_of_sales_accounts, net_overhead_costs_accounts

    @api.multi
    def get_list_date_intervals(self):
        self.ensure_one()
        result = [self.compute_dates_from_to()]
        if self.detail_per_month and self.several_months:
            result += self.get_months_list()
        return result

    @api.multi
    def write_on_multiple_cols(self, worksheet, line_num, column_num, data, style, merge=False):
        self.ensure_one()
        if self.compare_budget:
            nb_columns = 3
            if merge:
                worksheet.merge_range(line_num, column_num, line_num, column_num + nb_columns - 1, data, style)
            else:
                for index in range(nb_columns):
                    worksheet.write(line_num, column_num + index, data, style)
        worksheet.write(line_num, column_num, data, style)

    @api.multi
    def fill_bilan_for_interval(self, worksheet, column_num, date_from, date_to, style_simple_float,
                                style_float_title_blue, style_float_title_orange,
                                style_title_blue_background_centered,
                                revenue_accounts, cost_of_sales_accounts, net_overhead_costs_accounts):
        self.ensure_one()
        revenue_dict, expense_dict = self.get_analytic_revenue_subtotals_for_period(date_from, date_to, separated=False)
        budget_data_dict = {}
        if self.compare_budget:
            budget_data_dict = self.get_budget_data_dict(date_from, date_to)
        if date_from[5:7] == date_to[5:7]:
            period_display = "%s/%s" % (date_from[5:7], date_from[:4])
        else:
            period_display = "%s/%s - %s/%s" % (date_from[5:7], date_from[:4], date_to[5:7], date_to[:4])
        line_num = 0
        self.write_on_multiple_cols(worksheet, line_num, column_num, period_display,
                                    style_title_blue_background_centered, merge=True)
        line_num += 1
        if self.compare_budget:
            worksheet.write(line_num, column_num, u"Real", style_title_blue_background_centered)
            worksheet.write(line_num, column_num + 1, u"Budget", style_title_blue_background_centered)
            worksheet.write(line_num, column_num + 2, u"Difference", style_title_blue_background_centered)
            line_num += 1
        total_revenue = sum(list(revenue_dict.values()))
        worksheet.write(line_num, column_num, total_revenue, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 1
        for account in revenue_accounts:
            amount_real = 0
            for account_revenue in revenue_dict:
                if account_revenue == account:
                    amount_real = revenue_dict[account]
                    break
            worksheet.write(line_num, column_num, amount_real, style_simple_float)
            if self.compare_budget:
                amount_budget = 0
                for account_budget in budget_data_dict:
                    if account_budget == account:
                        amount_budget = budget_data_dict[account]
                        break
                if amount_budget:
                    worksheet.write(line_num, column_num + 1, amount_budget, style_simple_float)
                    worksheet.write(line_num, column_num + 2, amount_real - amount_budget, style_simple_float)
            line_num += 1
        line_num += 2
        total_cost_of_sales = 0
        for account_expense in expense_dict:
            if account_expense in cost_of_sales_accounts:
                total_cost_of_sales += expense_dict[account_expense]
        worksheet.write(line_num, column_num, total_cost_of_sales, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 1
        for account in cost_of_sales_accounts:
            amount = 0
            for account_expense in expense_dict:
                if account_expense == account:
                    amount = expense_dict[account]
                    break
            worksheet.write(line_num, column_num, amount, style_simple_float)
            line_num += 1
        profitability_coefficient = 0
        if total_cost_of_sales:
            profitability_coefficient = float((-1) * total_revenue) / total_cost_of_sales
        worksheet.write(line_num, column_num, profitability_coefficient, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 3
        gross_profit = total_revenue + total_cost_of_sales
        worksheet.write(line_num, column_num, gross_profit, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 3
        profit_loss_before_operating_expenses = gross_profit
        worksheet.write(line_num, column_num, profit_loss_before_operating_expenses, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 3
        total_net_overhead_costs = 0
        budget_net_overhead_costs = 0
        for account_expense in expense_dict:
            if account_expense in net_overhead_costs_accounts:
                total_net_overhead_costs += expense_dict[account_expense]
        for account_expense in net_overhead_costs_accounts:
            if account_expense in budget_data_dict:
                budget_net_overhead_costs += budget_data_dict[account_expense]
        difference_net_overhead_costs = total_net_overhead_costs - budget_net_overhead_costs
        worksheet.write(line_num, column_num, total_net_overhead_costs, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, budget_net_overhead_costs, style_float_title_blue)
            worksheet.write(line_num, column_num + 2, difference_net_overhead_costs, style_float_title_blue)
        line_num += 1
        for account in net_overhead_costs_accounts:
            amount_real = 0
            for account_expense in expense_dict:
                if account_expense == account:
                    amount_real = expense_dict[account]
                    break
            worksheet.write(line_num, column_num, amount_real, style_simple_float)
            if self.compare_budget:
                amount_budget = 0
                for account_budget in budget_data_dict:
                    if account_budget == account:
                        amount_budget = budget_data_dict[account]
                        break
                worksheet.write(line_num, column_num + 1, amount_budget, style_simple_float)
                worksheet.write(line_num, column_num + 2, amount_real - amount_budget, style_simple_float)
            line_num += 1
        line_num += 2
        ebitda = profit_loss_before_operating_expenses + total_net_overhead_costs
        worksheet.write(line_num, column_num, ebitda, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 2
        worksheet.write(line_num, column_num, 0, style_simple_float)
        line_num += 3
        operation_profit_loss = ebitda
        worksheet.write(line_num, column_num, operation_profit_loss, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        line_num += 2
        tva_reelle, tva_reconciliee = self.get_tax_amount_between_dates(date_from, date_to)
        worksheet.write(line_num, column_num, tva_reelle, style_simple_float)
        line_num += 1
        worksheet.write(line_num, column_num, tva_reconciliee, style_simple_float)
        line_num += 3
        profit_loss = operation_profit_loss
        worksheet.write(line_num, column_num, profit_loss, style_float_title_blue)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_blue)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_blue)
        final_profit_loss = profit_loss
        profit_shares = self.env['acm.profit.share'].search([])
        if profit_shares:
            line_num += 1
            for profit_share in profit_shares:
                line_num += 1
                amount = 0
                if profit_loss > 0:
                    amount = float(profit_loss * profit_share.percentage) / 100
                    final_profit_loss -= amount
                worksheet.write(line_num, column_num, amount, style_simple_float)
        line_num += 3
        worksheet.write(line_num, column_num, final_profit_loss, style_float_title_orange)
        if self.compare_budget:
            worksheet.write(line_num, column_num + 1, u"", style_float_title_orange)
            worksheet.write(line_num, column_num + 2, u"", style_float_title_orange)
        # Élargissement des colonnes
        worksheet.set_column(column_num, column_num, 12, None)
        if self.compare_budget:
            worksheet.set_column(column_num + 1, column_num + 1, 12, None)
            worksheet.set_column(column_num + 2, column_num + 2, 12, None)

    @api.multi
    def generate_bilan_worksheet(self, workbook, style_title_blue_background, style_title_blue_background_full_border,
                                 style_title_orange_background, style_title_orange_background_centered,
                                 style_simple_text, style_simple_float, style_float_title_blue,
                                 style_float_title_orange, style_title_blue_background_centered):
        self.ensure_one()
        worksheet = workbook.add_worksheet(u"Monthly")
        revenue_accounts, cost_of_sales_accounts, net_overhead_costs_accounts = self. \
            generate_frame_bilan_worksheet(worksheet, style_title_blue_background_full_border,
                                           style_title_orange_background, style_title_orange_background_centered,
                                           style_simple_text, style_title_blue_background_centered)
        column_num = 1
        for date_from, date_to in self.get_list_date_intervals():
            column_num += 1
            self.fill_bilan_for_interval(worksheet, column_num, date_from, date_to, style_simple_float,
                                         style_float_title_blue, style_float_title_orange,
                                         style_title_blue_background_centered,
                                         revenue_accounts, cost_of_sales_accounts, net_overhead_costs_accounts)
            if self.compare_budget:
                column_num += 2
        # On fige les cellules appropriées
        last_line_to_freeze = 1
        last_column_to_freeze = 3
        if self.compare_budget:
            last_line_to_freeze += 1
            last_column_to_freeze += 2
        worksheet.freeze_panes(last_line_to_freeze, last_column_to_freeze)

    @api.multi
    def launch_report(self):
        self.ensure_one()
        file_name = 'bilan_%s_%s_%s_%s' % (self.month_from, self.year_from, self.month_to, self.year_to)
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        style_title_blue_background = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'left',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
            'left': True,
            'right': True,
        })
        style_title_blue_background_full_border = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'left',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
            'left': True,
            'right': True,
        })
        style_title_blue_background_centered = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'center',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
            'left': True,
            'right': True,
        })
        style_title_orange_background = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'left',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#edca85',
            'top': True,
            'bottom': True,
        })
        style_title_orange_background_centered = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'center',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#edca85',
            'top': True,
            'bottom': True,
        })
        style_simple_text = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'left',
            'text_wrap': True,
        })
        style_simple_float = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'num_format': '#,##0.00',
        })
        style_float_title_blue = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'num_format': '#,##0.00',
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
        })
        style_float_title_orange = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'num_format': '#,##0.00',
            'bold': True,
            'bg_color': '#edca85',
            'top': True,
            'bottom': True,
        })
        self.generate_bilan_worksheet(workbook, style_title_blue_background, style_title_blue_background_full_border,
                                      style_title_orange_background, style_title_orange_background_centered,
                                      style_simple_text, style_simple_float, style_float_title_blue,
                                      style_float_title_orange, style_title_blue_background_centered)
        workbook.close()
        data = output.getvalue()
        attachment = self.create_attachment(base64.encodestring(data), file_name + '.xlsx')
        url = "/web/content/%s/%s?download=true" % (attachment.id, attachment.name)
        return {
            'type': 'ir.actions.act_url',
            'url': url,
        }
