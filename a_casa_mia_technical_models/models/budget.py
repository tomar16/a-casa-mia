# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import models, fields


class AcmCrossoveredBudgetLines(models.Model):
    _inherit = 'crossovered.budget.lines'

    planned_amount = fields.Float(help=u"Ce montant doit être négatif pour les dépenses, et positif pour les recettes")
    theoretical_amount = fields.Float(string=u"Practical Amount At Date")
    percentage = fields.Float(string=u"Ratio real/theoretical (%)")
