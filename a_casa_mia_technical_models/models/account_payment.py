# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import models, fields, api


class AcmAccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.model
    def create(self, vals):
        if not vals.get('payment_method_id'):
            payment_type = vals.get('payment_type')
            if payment_type:
                payment_method = self.env['account.payment.method']. \
                    search([('payment_type', '=', payment_type)], limit=1)
                if payment_method:
                    vals['payment_method_id'] = payment_method.id
        return super(AcmAccountPayment, self).create(vals)
