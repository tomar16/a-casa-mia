# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import base64
import logging
from io import BytesIO

import xlsxwriter

from odoo import models, api, exceptions

_logger = logging.getLogger(__name__)


class AcmBilanLauncher(models.TransientModel):
    _name = 'acm.expense.evolution.launcher'
    _inherit = ['quick.create.ir.attachment.abstract', 'acm.report.launcher.abstract']
    _description = u"Évolution des dépenses"

    @api.multi
    def fill_worksheet(self, workbook, worksheet_name, style_title_blue_background, style_float_title_blue,
                       style_simple_float, style_title_blue_background_centered, style_title_orange_background,
                       style_float_title_orange, type):
        worksheet = workbook.add_worksheet(worksheet_name)
        expense_accounts = self.env['account.analytic.account']
        data_by_month = {}
        total_revenue = 0
        dict_total_by_account = {}
        months_list = self.get_months_list()
        if len(months_list) <= 1:
            raise exceptions.UserError(u"Il n'y a rien à comparer")
        nb_months = len(months_list)
        for date_from, date_to in months_list:
            revenue_month = 0
            income_dict, expense_dict = self.get_analytic_revenue_subtotals_for_period(date_from, date_to,
                                                                                       separated=False)
            for analytic_account in income_dict:
                revenue_month += income_dict[analytic_account]
            for analytic_account in expense_dict:
                expense_dict[analytic_account] = (-1) * expense_dict[analytic_account]
                old_total_by_account = dict_total_by_account.get(analytic_account, 0)
                dict_total_by_account[analytic_account] = old_total_by_account + expense_dict[analytic_account]
                expense_accounts |= analytic_account
            data_by_month[(date_from, date_to)] = {'revenue': revenue_month, 'expenses': expense_dict}
            total_revenue += revenue_month
        line_num = 1
        column_num = 0
        # Élargissement des colonnes
        worksheet.set_column(column_num, column_num, 40, None)
        worksheet.write(line_num, column_num, u"Chiffre d'affaire", style_title_orange_background)
        line_num += 1
        for account in expense_accounts:
            worksheet.write(line_num, column_num, account.name, style_title_orange_background)
            line_num += 1
        worksheet.write(line_num, column_num, u"Total", style_title_orange_background)
        line_num += 1
        column_num += 1
        line_num = 0
        worksheet.set_column(column_num, column_num, 25, None)
        worksheet.write(line_num, column_num, u"Moyenne mensuelle", style_title_orange_background)
        line_num += 1
        average_revenue_by_month = float(total_revenue) / nb_months
        worksheet.write(line_num, column_num, average_revenue_by_month, style_float_title_orange)
        line_num += 1
        dict_average_expense_by_account = {}
        average_expense_total = 0
        for account in expense_accounts:
            total_for_account = dict_total_by_account.get(account, 0)
            average_expense_for_account = float(total_for_account) / nb_months
            average_expense_total += average_expense_for_account
            dict_average_expense_by_account[account] = average_expense_for_account
            worksheet.write(line_num, column_num, average_expense_for_account, style_simple_float)
            line_num += 1
        worksheet.write(line_num, column_num, average_expense_total, style_float_title_orange)
        column_num += 1
        for date_from, date_to in months_list:
            total_amount_for_month = 0
            total_ecart_for_month = 0
            line_num = 0
            worksheet.set_column(column_num, column_num, 12, None)
            month_display = "%s/%s" % (date_from[5:7], date_from[:4])
            worksheet.write(line_num, column_num, month_display, style_title_blue_background_centered)
            line_num += 1
            total_revenue_month = data_by_month.get((date_from, date_to), {}).get('revenue', 0)
            dict_expenses_month = data_by_month.get((date_from, date_to), {}).get('expenses', 0)
            revenue_month_amount = 0
            ecart_revenue_month = total_revenue_month - average_revenue_by_month
            if type == 'montants':
                revenue_month_amount = total_revenue_month
            if type == 'ecarts_montants':
                revenue_month_amount = ecart_revenue_month
            if type == 'ecarts_montants_pourcentage':
                if average_revenue_by_month == 0:
                    revenue_month_amount = 0
                else:
                    revenue_month_amount = float(total_revenue_month * 100) / average_revenue_by_month
            worksheet.write(line_num, column_num, revenue_month_amount, style_float_title_blue)
            line_num += 1
            for account in expense_accounts:
                expense_for_account = dict_expenses_month.get(account, 0)
                average_expense = dict_average_expense_by_account.get(account, 0)
                ecart_montant = expense_for_account - average_expense
                amount = 0
                total_amount_for_month += expense_for_account
                if type == 'montants':
                    amount = expense_for_account
                if type == 'ecarts_montants':
                    amount = ecart_montant
                    total_ecart_for_month += amount
                if type == 'ecarts_montants_pourcentage':
                    if average_expense == 0:
                        amount = 0
                    else:
                        amount = float(expense_for_account * 100) / average_expense

                worksheet.write(line_num, column_num, amount, style_simple_float)
                line_num += 1
            amount = 0
            if type == 'montants':
                amount = total_amount_for_month
            if type == 'ecarts_montants':
                amount = total_ecart_for_month
            if type == 'ecarts_montants_pourcentage':
                if average_expense_total:
                    amount = float(total_amount_for_month * 100) / average_expense_total
            worksheet.write(line_num, column_num, amount, style_float_title_blue)
            column_num += 1
        # On fige les cellules appropriées
        last_line_to_freeze = 2
        last_column_to_freeze = 2
        worksheet.freeze_panes(last_line_to_freeze, last_column_to_freeze)

    @api.multi
    def launch_report(self):
        self.ensure_one()
        file_name = 'expense_evolution_%s_%s_%s_%s' % (self.month_from, self.year_from, self.month_to, self.year_to)
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        style_title_blue_background = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'left',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
            'left': True,
            'right': True,
        })
        style_title_blue_background_centered = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'center',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
            'left': True,
            'right': True,
        })
        style_title_orange_background = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'align': 'left',
            'text_wrap': True,
            'bold': True,
            'bg_color': '#edca85',
            'top': True,
            'bottom': True,
        })
        style_simple_float = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'num_format': '#,##0.00',
        })
        style_float_title_blue = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'num_format': '#,##0.00',
            'bold': True,
            'bg_color': '#bccff5',
            'top': True,
            'bottom': True,
        })
        style_float_title_orange = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'num_format': '#,##0.00',
            'bold': True,
            'bg_color': '#edca85',
            'top': True,
            'bottom': True,
        })
        self.fill_worksheet(workbook, u"Évolution dépenses", style_title_blue_background, style_float_title_blue,
                            style_simple_float, style_title_blue_background_centered, style_title_orange_background,
                            style_float_title_orange, type='montants')
        self.fill_worksheet(workbook, u"Écarts dépenses (montants)", style_title_blue_background,
                            style_float_title_blue, style_simple_float, style_title_blue_background_centered,
                            style_title_orange_background, style_float_title_orange, type='ecarts_montants')
        self.fill_worksheet(workbook, u"Écarts dépenses (pourcentages)", style_title_blue_background,
                            style_float_title_blue, style_simple_float, style_title_blue_background_centered,
                            style_title_orange_background, style_float_title_orange, type='ecarts_montants_pourcentage')
        workbook.close()
        data = output.getvalue()
        attachment = self.create_attachment(base64.encodestring(data), file_name + '.xlsx')
        url = "/web/content/%s/%s?download=true" % (attachment.id, attachment.name)
        return {
            'type': 'ir.actions.act_url',
            'url': url,
        }
