# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import csv
import logging

from odoo.exceptions import UserError

from odoo import api, models, exceptions, _

TMB_CAISSE_ACCOUNT_NUMBER = u"00017-11000-50442220001-53"
TMB_CAISSE_CURRENCY = u"USD"

DICT_MONTHS = {
    'Jan': '01',
    'Feb': '02',
    'Mar': '03',
    'Apr': '04',
    'May': '05',
    'Jun': '06',
    'Jul': '07',
    'Aug': '08',
    'Sep': '09',
    'Oct': '10',
    'Nov': '11',
    'Dec': '12',
}

_logger = logging.getLogger(__name__)


class AcmBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    @api.model
    def _check_tmb_caisse(self, data_file):
        try:
            csv_file_decoded = data_file.decode('latin-1')
            csv_file = csv.reader(csv_file_decoded.split('\n'), delimiter=',')
            csv_file = [[row for row in line] for line in csv_file]
            for line in csv_file:
                print('line', line)
                if line and line[1] == TMB_CAISSE_ACCOUNT_NUMBER:
                    return csv_file
        except Exception as e:
            return False

    @api.model
    def _prepare_tmb_pos_transaction_line(self, transaction):
        date, _, name, ref, _, amount_str, _, _  = transaction[:7]
        number_format = len(amount_str) >= 2 and amount_str[-2:] or ''
        if len(amount_str) < 2 or amount_str[-2:] not in ['Cr', 'Dr']:
            raise exceptions.UserError(u"Format de chiffre non reconnu : %s" % number_format)
        month = DICT_MONTHS.get(date[3:6])
        if not month:
            raise exceptions.UserError(u"Format de mois non reconnu : %s" % date[3:6])
        vals = {
            'date': '%s-%s-%s' % (date[7:11], month, date[:2]),
            'name': name,
            'ref': ref,
            'amount': (number_format == 'Cr' and 1 or -1) * float(amount_str[:-2] or '0'),
        }
        return vals

    def _parse_file(self, data_file):
        print('_parse_file')
        tmb_caisse = self._check_tmb_caisse(data_file)
        if not tmb_caisse:
            return super(AcmBankStatementImport, self)._parse_file(data_file)

        transactions = []
        balance_start = 0.00
        balance_end = 0.00
        try:
            index = 0
            for transaction in tmb_caisse:
                index += 1
                if index == 2:
                    balance_start = float(transaction[1].replace(TMB_CAISSE_CURRENCY, '').replace(' ', ''))
                if index == 3:
                    balance_end = float(transaction[1].replace(TMB_CAISSE_CURRENCY, '').replace(' ', ''))
                if not transaction:
                    continue
                if len(transaction[0]) != 11:
                    continue
                vals = self._prepare_tmb_pos_transaction_line(transaction)
                if vals:
                    transactions.append(vals)
        except Exception as e:
            raise UserError(_(
                "The following problem occurred during import. "
                "The file might not be valid.\n\n %s") % e.message)

        vals_bank_statement = {
            'name': TMB_CAISSE_ACCOUNT_NUMBER,
            'transactions': transactions,
            'balance_start': balance_start,
            'balance_end_real': balance_end,
        }
        return TMB_CAISSE_CURRENCY, TMB_CAISSE_ACCOUNT_NUMBER, [vals_bank_statement]
