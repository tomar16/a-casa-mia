# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import csv
import logging

from odoo.exceptions import UserError

from odoo import api, models

RAWBANK_POS_ACCOUNT_NUMBER = u"05101-05509404401-25 USD"
RAWBANK_POS_CURRENCY = u"USD"

_logger = logging.getLogger(__name__)


class AcmBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    @api.model
    def _check_rawbank_pos(self, data_file):
        try:
            csv_file_decoded = data_file.decode('latin-1')
            dialect = csv.Sniffer().sniff(csv_file_decoded)
            csv_file = csv.reader(csv_file_decoded.split('\n'), dialect, delimiter=';')
            csv_file = [[row for row in line] for line in csv_file]
            for line in csv_file:
                if line and line[1] == RAWBANK_POS_ACCOUNT_NUMBER:
                    return csv_file
        except Exception as e:
            return False

    def _parse_file(self, data_file):
        rawbank_pos = self._check_rawbank_pos(data_file)
        if not rawbank_pos:
            return super(AcmBankStatementImport, self)._parse_file(data_file)

        transactions = []
        final_transactions = []
        total_amt = 0.00
        balance_end_real = 0.00
        try:
            last_vals = {}
            first_line_reached = False
            for transaction in rawbank_pos:
                if not transaction:
                    _logger.info(u"Empty line, skipped")
                    continue
                # On passe les lignes d'en-tête
                if transaction[0] == u"Date" and transaction[1] == u"Valeur":
                    first_line_reached = True
                    continue
                if not first_line_reached:
                    _logger.info(u"Skipping line %s", transaction)
                    continue
                # On passe la dernière ligne
                if u"Solde (USD) au" in transaction[2]:
                    continue
                date, _, name, debit, credit, end_balance = transaction[:6]
                # Column date is sometimes filled with a blank
                date = date.replace(' ', '')
                if not date and name and last_vals:
                    # Certains libellés sont sur plusieurs lignes : il faut les regrouper dans la même ligne de relevé
                    last_vals['name'] = u"%s / %s" % (last_vals['name'], name)
                    continue
                if date and last_vals:
                    # Lorsqu'on tombe sur une nouvelle date, on valide les lignes créées
                    _logger.info(u"Add new bank statement line with data %s", last_vals)
                    transactions.append(last_vals)
                    last_vals = {}
                if date:
                    # Initialisation d'une nouvelle ligne
                    balance_end_real = float(end_balance.replace(',', '.') or '0')
                    splitted_date = date.split('/')
                    if len(splitted_date) != 3:
                        raise UserError(u"Ligne %s : format de date non reconnu (%s)" % (name, date))
                    day, month, year = splitted_date
                    day = int(day)
                    month = int(month)
                    year = int(year)
                    if year < 100:
                        year += 2000
                    last_vals = {
                        'date': '%s-%s-%s' % (year, '%02d' % month, '%02d' % day),
                        'name': name,
                        'ref': u"",
                        'amount': float(credit.replace(',', '.') or '0') - float(debit.replace(',', '.') or '0'),
                    }
                    total_amt += last_vals['amount']
            # Ajout de la dernière ligne
            if last_vals:
                _logger.info(u"Add new bank statement line with data %s", last_vals)
                transactions.append(last_vals)
            # We remove null lines
            for transaction in transactions:
                if transaction.get('amount', 0) == 0:
                    _logger.info(u"Removing null statement line with data %s", transaction)
                    continue
                final_transactions.append(transaction)
        except Exception as e:
            raise UserError(_(
                "The following problem occurred during import. "
                "The file might not be valid.\n\n %s") % e.message)

        vals_bank_statement = {
            'name': RAWBANK_POS_ACCOUNT_NUMBER,
            'transactions': final_transactions,
            'balance_start': balance_end_real - total_amt,
            'balance_end_real': balance_end_real,
        }
        return RAWBANK_POS_CURRENCY, RAWBANK_POS_ACCOUNT_NUMBER, [vals_bank_statement]
