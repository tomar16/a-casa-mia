# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import base64
import codecs
import csv
import logging

from odoo.tools import float_compare

from odoo import models, fields, api, exceptions

_logger = logging.getLogger(__name__)


class AcmAccountHistoryImportation(models.Model):
    _name = 'acm.account.history.importation'
    _description = "Import de l'historique comptable"
    _order = 'id desc'

    file = fields.Binary(string=u"Fichier à importer", attachment=True, required=True)
    rate = fields.Float(string=u"Taux", help=u"Combien de dollars pour un euro ?", digits=(12, 6),
                        required=True, default=1.05)
    logs = fields.Text(string=u"Logs", readonly=True)

    @api.multi
    def name_get(self):
        return [(rec.id, u"Fichier importé en date du %s (UTC)" % rec.create_date) for rec in self]

    @api.model
    def add_log(self, final_logs, new_message):
        _logger.info(new_message)
        if final_logs:
            final_logs += u"""\n"""
        final_logs += new_message
        return final_logs

    @api.multi
    def check_data(self, row):
        self.ensure_one()
        if len(row) != 8:
            raise exceptions.UserError(u"Le fichier d'import doit comporter exactement 8 colonnes : %s." % row)
        move_id, date, journal_code, account_code, name, comment, line_amount, line_type = row
        line_amount = float(line_amount and line_amount.replace(',', '.') or '0') or 0
        if not line_amount:
            return move_id, date, journal_code, account_code, name, comment, line_amount, line_type
        if line_amount < 0:
            if line_type == 'C':
                line_amount = (-1) * line_amount
                line_type = 'D'
            elif line_type == 'D':
                line_amount = (-1) * line_amount
                line_type = 'C'
        if not move_id:
            raise exceptions.UserError(u"L'ID de l'écriture comptable est obligatoire")
        if not date:
            raise exceptions.UserError(u"Écriture %s : la date est obligatoire" % move_id)
        if len(date) != 6:
            raise exceptions.UserError(u"Écriture %s : la date doit comporter 6 chiffres" % move_id)
        if not journal_code:
            raise exceptions.UserError(u"Écriture %s : le code du journal comptable est obligatoire" % move_id)
        if not account_code:
            raise exceptions.UserError(u"Écriture %s : le code du compte comptable est obligatoire" % move_id)
        if not account_code.startswith('7') and not account_code.startswith('445') and \
                not account_code.startswith('5') and not account_code.startswith('4'):
            raise exceptions.UserError(u"Écriture %s : le code du compte comptable doit commencer par 7 (produit de la "
                                       u"vente), 445 (TVA), 5 (règlement) ou 4 (tiers). Ici, il vaut %s" %
                                       (move_id, account_code))
        if not name:
            raise exceptions.UserError(u"Écriture %s : le libellé de la ligne est obligatoire" % move_id)
        if not line_type:
            raise exceptions.UserError(u"Écriture %s : le type de la ligne est obligatoire" % move_id)
        if line_type not in ['C', 'D']:
            raise exceptions.UserError(u"Écriture %s : le type de la ligne (colonne 8) doit être rempli par l'une des "
                                       u"valeurs suivantes : C (crédit) ou D (débit)" % move_id)
        return move_id, date, journal_code, account_code, name, comment, line_amount, line_type

    @api.multi
    def compute_invoice_data(self, move_id, journal_code, final_date):
        self.ensure_one()
        unknow_customer_id = self.env.ref('a_casa_mia_config.a_casa_mia_unknown_customer').id
        payment_term_immediate_id = self.env.ref('account.account_payment_term_immediate').id
        journal = self.env['account.journal'].search([('code', '=', journal_code),
                                                      ('company_id', '=', self.env.user.company_id.id)])
        if not journal or len(journal) > 1:
            raise exceptions.UserError(u"Aucun journal trouvé pour le code %s" % journal_code)
        return {'partner_id': unknow_customer_id,
                'date_invoice': final_date,
                'date_due': final_date,
                'user_id': self.env.user.id,
                'journal_id': journal.id,
                'currency_id': self.env.ref('base.USD').id,
                'payment_term_id': payment_term_immediate_id,
                'origin': u"ID dans le fichier source : %s" % move_id,
                'name': u"Numéro de l'import : %s" % self.id,
                'type': 'out_invoice',
                'imported_from_file_id': self.id}

    @api.model
    def compute_invoice_line_data(self, line_type, invoice, account_code, name, line_amount, final_logs):
        if line_type == 'C':
            invoice_line_amount = line_amount
        else:
            invoice_line_amount = (-1) * line_amount
        account = self.env['account.account'].search([('code', '=', account_code),
                                                      ('company_id', '=', self.env.user.company_id.id)],
                                                     limit=1)
        if not account:
            template_account = self.env['account.account'].search([('code', '=', '707100')], limit=1)
            if not template_account:
                raise exceptions.UserError(u"Compte 707100 (modèle de compte de vente, servant pour la création"
                                           u" d'un nouveau compte) non trouvé")
            vals_copy = {'code': account_code, 'name': name}
            final_logs = self.add_log(final_logs, u"Duplicate account 717100 with data %s" % vals_copy)
            account = template_account.copy(vals_copy)
        product = self.env['product.product'].search([('name', '=', name)], limit=1)
        product.write({'sale_ok': True})
        if not product:
            product_data = {'name': name,
                            'property_account_income_id': account.id,
                            'lst_price': abs(line_amount),
                            'sale_ok': True,
                            'purchase_ok': True}
            final_logs = self.add_log(final_logs, u"Creating product with data %s" % product_data)
            product = self.env['product.product'].create(product_data)
        elif product.property_account_income_id != account:
            product.property_account_income_id = account
        invoice_line_data = {
            'invoice_id': invoice.id,
            'product_id': product.id,
            'name': name,
            'account_id': account.id,
            'account_analytic_id': self.env.ref('fill_empty_analytic_accounts.analytic_account_sales').id,
            'quantity': 1,
            'price_unit': invoice_line_amount,
            'invoice_line_tax_ids': [(6, 0, self.env.ref('l10n_fr.1_tva_normale').ids)],
        }
        return invoice_line_data, final_logs

    @api.model
    def process_vat_difference(self, invoice, invoice_vat, target_vat, move_id, final_logs):
        final_logs = self.add_log(final_logs, u"Correcting VAT from %s to %s for invoice ID=%s" %
                                  (invoice_vat, target_vat, invoice.id))
        if len(invoice.tax_line_ids) > 1:
            invoice.tax_line_ids[1:].unlink()
        if not invoice.tax_line_ids:
            account = self.env['account.account'].search([('code', '=', '445000'),
                                                          ('company_id', '=', self.env.user.company_id.id)],
                                                         limit=1)
            if not account:
                raise exceptions.UserError(u"Compte 445000 (TVA) non trouvé")
            self.env['account.invoice.tax'].create({
                'invoice_id': invoice.id,
                'name': "TVA 16%",
                'account_id': account.id,
                'amount': target_vat,
                'manual': True,
            })
        else:
            invoice.tax_line_ids.write({
                'amount': target_vat,
                'manual': True,
            })
        invoice_vat = invoice.amount_tax
        if round(invoice_vat, 2) != round(target_vat, 2):
            raise exceptions.UserError(u"Pour l'écriture d'ID %s, la TVA devrait être de %s, mais elle "
                                       u"est de %s" % (move_id, target_vat, invoice_vat))
        return final_logs

    @api.model
    def compute_payment_sign(self, invoice, payment_data):
        sign = 1
        if invoice.type == 'out_invoice' and payment_data['line_type'] == 'C':
            sign = -1
        if invoice.type == 'out_refund' and payment_data['line_type'] == 'D':
            sign = -1
        return sign

    @api.model
    def get_payment_journal(self, payment_data, final_logs):
        journal = self.env['account.journal']. \
            search([('is_payment_method', '=', True),
                    ('name', '=', payment_data['payment_mode_name'])])
        if not journal:
            account = self.env['account.account'].search([('code', '=', payment_data['account_code']),
                                                          ('company_id', '=', self.env.user.company_id.id)],
                                                         limit=1)
            if not account:
                template_account = self.env['account.account'].search([('code', '=', '531000')], limit=1)
                if not template_account:
                    raise exceptions.UserError(u"Compte 531000 (modèle de compte de règlement, servant pour la "
                                               u"création d'un nouveau compte) non trouvé")
                vals_copy = {'code': payment_data['account_code'], 'name': payment_data['payment_mode_name']}
                final_logs = self.add_log(final_logs, u"Duplicate account 531000 with data %s" % vals_copy)
                account = template_account.copy(vals_copy)
            inbound_methods = self.env['account.payment.method'].search([('payment_type', '=', 'inbound')])
            outbound_methods = self.env['account.payment.method'].search([('payment_type', '=', 'outbound')])
            journal_data = {
                'name': payment_data['payment_mode_name'],
                'default_debit_account_id': account.id,
                'default_credit_account_id': account.id,
                'is_payment_method': True,
                'code': payment_data['payment_mode_name'].replace(" ", "")[:3].upper(),
                'type': 'bank',
                'inbound_payment_method_ids': [(6, 0, inbound_methods.ids)],
                'outbound_payment_method_ids': [(6, 0, outbound_methods.ids)],
                'show_on_dashboard': False,
            }
            final_logs = self.add_log(final_logs, u"Creating journal with data %s" % journal_data)
            journal = self.env['account.journal'].create(journal_data)
        return journal, final_logs

    @api.model
    def create_and_validate_payment(self, invoice, sign, journal, payment_data, final_logs):
        payment_method_id = invoice.type in ['out_invoice', 'in_refund'] and \
                            self.env.ref('account.account_payment_method_manual_in').id or \
                            self.env.ref('account.account_payment_method_manual_out').id
        payment_obj_data = {
            'amount': sign * payment_data['amount'],
            'currency_id': self.env.ref('base.USD').id,
            'journal_id': journal.id,
            'payment_date': payment_data['date'],
            'communication': u"ID dans le fichier source : %s" % payment_data['move_id'],
            'invoice_ids': [(6, 0, invoice.ids)],
            'payment_type': invoice.type in ['out_invoice', 'in_refund'] and 'inbound' or 'outbound',
            'payment_method_id': payment_method_id,
            'partner_type': 'customer'
        }
        final_logs = self.add_log(final_logs, u"Creating payment with data %s" % payment_obj_data)
        payment = self.env['account.payment'].create(payment_obj_data)
        invoice.payment_mode_free_string = payment_data['payment_mode_name']
        final_logs = self.add_log(final_logs, u"Payment mode of invoice ID=%s (%s) set to %s" %
                                  (invoice.id, invoice.number, payment_data['payment_mode_name']))
        payment.action_validate_invoice_payment()
        final_logs = self.add_log(final_logs, u"Final invoice state: %s" % invoice.state)
        return final_logs

    @api.model
    def get_final_action(self, created_invoice_ids):
        return {
            'name': u"Nouvelles factures importées",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'domain': [('id', 'in', created_invoice_ids)],
            'context': self.env.context,
            'views': [
                (self.env.ref('account.invoice_tree_with_onboarding').id, 'tree'),
                (self.env.ref('account.invoice_form').id, 'form'),
            ],
        }

    @api.model
    def change_line_rounding_for_not_balanced_moves(self, dict_line_data_by_move, final_logs):
        for move_id in dict_line_data_by_move:
            sum_debit = 0
            sum_credit = 0
            strongest_line_account_code = False
            strongest_line_amount = 0
            strongest_line_type = 'D'
            for move_line_data in dict_line_data_by_move[move_id]:
                account_code = move_line_data['account_code']
                line_amount = move_line_data['line_amount']
                line_type = move_line_data['line_type']
                if line_type == 'C':
                    sum_credit += line_amount
                if line_type == 'D':
                    sum_debit += line_amount
                if not line_amount or line_amount > strongest_line_amount:
                    strongest_line_account_code = account_code
                    strongest_line_amount = line_amount
                    strongest_line_type = line_type
            sum_debit = round(sum_debit, 2)
            sum_credit = round(sum_credit, 2)
            # Pour les écritures contenant une ligne de vente, on modifie la première ligne de vente trouvée
            if sum_credit != sum_debit:
                for move_line_data in dict_line_data_by_move[move_id]:
                    account_code = move_line_data['account_code']
                    line_amount = move_line_data['line_amount']
                    line_type = move_line_data['line_type']
                    if account_code.startswith('7'):
                        new_amount = line_amount
                        if line_type == 'C':
                            new_amount += (sum_debit - sum_credit)
                            sum_credit += (sum_debit - sum_credit)
                        if line_type == 'D':
                            new_amount += (sum_credit - sum_debit)
                            sum_debit += (sum_credit - sum_debit)
                        new_amount = round(new_amount, 2)
                        msg = u"Move ID=%s (sale type) not balanced, amount of line with account %s changed " \
                              u"from %s to %s"
                        final_logs = self.add_log(final_logs, msg % (move_id, account_code, line_amount, new_amount))
                        move_line_data['line_amount'] = new_amount
                        break
            # Pour les autres, on modifie la ligne dont le montant est le plus grand
            if sum_credit != sum_debit:
                missing_debit = sum_credit - sum_debit
                for move_line_data in dict_line_data_by_move[move_id]:
                    account_code = move_line_data['account_code']
                    line_amount = move_line_data['line_amount']
                    line_type = move_line_data['line_type']
                    if account_code == strongest_line_account_code and \
                            line_amount == strongest_line_amount and \
                            line_type == strongest_line_type:
                        new_amount = line_amount
                        if line_type == 'D':
                            new_amount += missing_debit
                        elif line_type == 'C':
                            new_amount -= missing_debit
                        new_amount = round(new_amount, 2)
                        msg = u"Move ID=%s (not sale type) not balanced, amount of line with account %s changed " \
                              u"from %s to %s"
                        final_logs = self.add_log(final_logs, msg % (move_id, account_code, line_amount, new_amount))
                        move_line_data['line_amount'] = new_amount
                        break
        return dict_line_data_by_move, final_logs

    @api.multi
    def import_file(self):
        self.ensure_one()
        _logger.info(u"Importing account history file")
        final_logs = u""""""
        binary_string = codecs.decode(base64.b64decode(self.file))
        dialect = csv.Sniffer().sniff(binary_string)
        file = csv.reader(binary_string.split('\n'), dialect, delimiter=';')
        index = 0
        dict_invoice_by_id = {}
        dict_vat_by_invoice = {}
        dict_payments_by_invoice = {}
        dict_line_data_by_move = {}
        for row in file:
            index += 1
            if not row:
                continue
            move_id, date, journal_code, account_code, name, comment, line_amount, line_type = self.check_data(row)
            move_id = int(move_id)
            if move_id not in dict_line_data_by_move:
                dict_line_data_by_move[move_id] = []
            dict_line_data_by_move[move_id] += [{
                'date': date,
                'journal_code': journal_code,
                'account_code': account_code,
                'name': name,
                'comment': comment,
                'line_amount': round(line_amount * self.rate, 2), # Conversion du fichier euro en dollar
                'line_type': line_type,
            }]
        dict_line_data_by_move, final_logs = self.change_line_rounding_for_not_balanced_moves(dict_line_data_by_move,
                                                                                              final_logs)
        move_ids = list(dict_line_data_by_move.keys())
        move_ids.sort()
        for move_id in move_ids:
            for move_line_data in dict_line_data_by_move[move_id]:
                final_logs = self.add_log(final_logs, u"Importing line move_id=%s, data %s" % (move_id, move_line_data))
                date = move_line_data['date']
                journal_code = move_line_data['journal_code']
                account_code = move_line_data['account_code']
                name = move_line_data['name']
                if account_code.startswith('7') and '-' in name:
                    name = name.split('-')[0]
                comment = move_line_data['comment']
                line_amount = move_line_data['line_amount']
                line_type = move_line_data['line_type']
                if not line_amount:
                    continue
                final_date = '20%s-%s-%s' % (date[4:6], date[2:4], date[0:2])
                invoice = dict_invoice_by_id.get(move_id)
                if not invoice:
                    invoice_data = self.compute_invoice_data(move_id, journal_code, final_date)
                    final_logs = self.add_log(final_logs, u"Creating invoice with data %s" % invoice_data)
                    invoice = self.env['account.invoice'].create(invoice_data)
                    dict_invoice_by_id[move_id] = invoice
                if account_code.startswith('445'):
                    # On somme les lignes de TVA, pour la vérification finale
                    if line_type == 'C':
                        dict_vat_by_invoice[invoice] = dict_vat_by_invoice.get(invoice, 0) + line_amount
                    else:
                        dict_vat_by_invoice[invoice] = dict_vat_by_invoice.get(invoice, 0) - line_amount
                elif account_code.startswith('7') or account_code.startswith('4'):
                    # Pour une ligne de vente (comptes 7), on crée une ligne de facture
                    # Les lignes des comptes 4 correspondent à des contreparties de remises
                    invoice_line_data, final_logs = self.compute_invoice_line_data(line_type, invoice, account_code,
                                                                                   name, line_amount, final_logs)
                    final_logs = self.add_log(final_logs, u"Creating invoice line with data %s" % invoice_line_data)
                    self.env['account.invoice.line'].create(invoice_line_data)
                elif account_code.startswith('5'):
                    if invoice not in dict_payments_by_invoice:
                        dict_payments_by_invoice[invoice] = []
                    dict_payments_by_invoice[invoice] += [{
                        'move_id': move_id,
                        'date': final_date,
                        'account_code': account_code,
                        'payment_mode_name': comment,
                        'amount': line_amount,
                        'line_type': line_type,
                    }]
        created_invoice_ids = []
        for move_id in dict_invoice_by_id:
            invoice = dict_invoice_by_id[move_id]
            # Passage en avoir des lignes négatives
            if float_compare(invoice.amount_total, 0.0, precision_rounding=invoice.currency_id.rounding) == -1:
                final_logs = self.add_log(final_logs, u"Invoice ID=%s switched to refund" % invoice.id)
                invoice.type = 'out_refund'
                for line in invoice.invoice_line_ids:
                    line.price_unit = (-1) * line.price_unit
            invoice.compute_taxes()
            invoice_vat = invoice.amount_tax
            target_vat = dict_vat_by_invoice.get(invoice, 0)
            if invoice.type == 'out_refund':
                target_vat = (-1) * target_vat
            if round(invoice_vat, 2) != round(target_vat, 2):
                final_logs = self.process_vat_difference(invoice, invoice_vat, target_vat, move_id, final_logs)
            if float_compare(invoice.amount_total, 0.0, precision_rounding=invoice.currency_id.rounding) == -1:
                raise exceptions.UserError(u"%s : impossible de générer une facture négative (montant : %s)" %
                                           (invoice.origin, invoice.amount_total))
            invoice.action_invoice_open()
            created_invoice_ids += [invoice.id]
        for invoice in dict_payments_by_invoice:
            for payment_data in dict_payments_by_invoice[invoice]:
                sign = self.compute_payment_sign(invoice, payment_data)
                journal, final_logs = self.get_payment_journal(payment_data, final_logs)
                final_logs = self.create_and_validate_payment(invoice, sign, journal, payment_data, final_logs)
        # We must save logs in sudo mode, because nobody can write this object.
        self.sudo().write({'logs': final_logs})
        return self.get_final_action(created_invoice_ids)
