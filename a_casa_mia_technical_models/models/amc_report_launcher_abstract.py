# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from dateutil.relativedelta import relativedelta

from odoo import models, fields, api

MONTH_SELECTION = [('%02d' % number, '%02d' % number) for number in range(1, 13)]

# Do not change year selection here, because it will impact existing data
YEAR_SELECTION = [('%02d' % number, '%02d' % number) for number in range(2019, 2051)]


class AmcMonthlyReportLauncher(models.AbstractModel):
    _name = 'acm.report.launcher.abstract'
    _description = u"Abstract model to launch ACM reports"

    current_month = fields.Boolean(string=u"Mois en cours")
    previous_month = fields.Boolean(string=u"Mois précédent")
    current_year = fields.Boolean(string=u"Année en cours")
    previous_year = fields.Boolean(string=u"Année précédente")
    month_from = fields.Selection(MONTH_SELECTION, string=u"Mois de début", required=True)
    year_from = fields.Selection(YEAR_SELECTION, string=u"Année de début", required=True)
    month_to = fields.Selection(MONTH_SELECTION, string=u"Mois de fin", required=True)
    year_to = fields.Selection(YEAR_SELECTION, string=u"Année de fin", required=True)
    several_months = fields.Boolean(string=u"Analyse comportant plusieurs mois", compute='_compute_several_months')

    @api.multi
    @api.depends('month_from', 'month_to', 'year_from', 'year_to')
    def _compute_several_months(self):
        for rec in self:
            rec.several_months = rec.month_from != rec.month_to or rec.year_from != rec.year_to

    @api.multi
    @api.onchange('current_month')
    def onchange_current_month(self):
        for rec in self:
            if rec.current_month:
                rec.previous_month = False
                rec.current_year = False
                rec.previous_year = False
                date_today = fields.Date.to_string(fields.Date.today())
                month = date_today[5:7]
                year = date_today[:4]
                rec.month_from = month
                rec.month_to = month
                rec.year_from = year
                rec.year_to = year

    @api.multi
    @api.onchange('previous_month')
    def onchange_previous_month(self):
        for rec in self:
            if rec.previous_month:
                rec.current_month = False
                rec.current_year = False
                rec.previous_year = False
                date_last_month = fields.Date.to_string(fields.Date.today() - relativedelta(months=1))
                month = date_last_month[5:7]
                year = date_last_month[:4]
                rec.month_from = month
                rec.month_to = month
                rec.year_from = year
                rec.year_to = year

    @api.multi
    @api.onchange('current_year')
    def onchange_current_year(self):
        for rec in self:
            if rec.current_year:
                rec.current_month = False
                rec.previous_month = False
                rec.previous_year = False
                date_today = fields.Date.to_string(fields.Date.today())
                month = date_today[5:7]
                year = date_today[:4]
                rec.month_from = '01'
                rec.month_to = month
                rec.year_from = year
                rec.year_to = year

    @api.multi
    @api.onchange('previous_year')
    def onchange_previous_year(self):
        for rec in self:
            if rec.previous_year:
                rec.current_month = False
                rec.previous_month = False
                rec.current_year = False
                date_today = fields.Date.to_string(fields.Date.today())
                year = str(int(date_today[:4]) - 1)
                rec.month_from = '01'
                rec.month_to = '12'
                rec.year_from = year
                rec.year_to = year

    @api.multi
    def compute_dates_from_to(self):
        self.ensure_one()
        month_to_first_day = '%s-%s-01' % (self.year_to, self.month_to)
        month_to_first_day = fields.Date.from_string(month_to_first_day)
        date_from = '%s-%s-01' % (self.year_from, self.month_from)
        date_to = fields.Date.to_string(month_to_first_day + relativedelta(months=1) - relativedelta(days=1))
        return date_from, date_to

    @api.model
    def get_analytic_subtotals_for_period(self, date_from, date_to):
        # TODO: reprendre plus proprement les libellés des types de comptes
        self.env.cr.execute("""WITH tax_accounts AS (
  SELECT account_id
  FROM account_tax
  WHERE coalesce(active, FALSE) IS TRUE
  UNION ALL
  SELECT refund_account_id AS account_id
  FROM account_tax
  WHERE coalesce(active, FALSE) IS TRUE),

     tax_amount_by_move AS (
       SELECT am.id             AS move_id,
              sum(CASE
                    WHEN aml.account_id IN (SELECT account_id FROM tax_accounts)
                      THEN coalesce(aml.credit, 0) - coalesce(aml.debit, 0)
                    ELSE 0 END) AS amount_tax,
              sum(CASE
                    WHEN aat.name NOT IN ('Receivable', 'Payable', 'Bank and Cash') AND
                         aml.account_id NOT IN (SELECT account_id FROM tax_accounts)
                      THEN coalesce(aml.credit, 0) - coalesce(aml.debit, 0)
                    ELSE 0 END) AS amount_untaxed
       FROM account_move am
              LEFT JOIN account_move_line aml ON aml.move_id = am.id
              LEFT JOIN account_account aa ON aa.id = aml.account_id
              LEFT JOIN account_account_type aat ON aat.id = aa.user_type_id
       GROUP BY am.id),

     detailed_result AS (
       SELECT aal.account_id                    AS analytic_account_id,
              aal.amount                        AS amount_untaxed,
              aal.amount * (1 + (CASE
                                   WHEN coalesce(ta.amount_untaxed, 0) != 0 THEN
                                     coalesce(ta.amount_tax, 0) / coalesce(ta.amount_untaxed, 0)
                                   ELSE 0 END)) AS amount_total
       FROM account_analytic_line aal
              LEFT JOIN account_move_line aml ON aml.id = aal.move_id
              LEFT JOIN tax_amount_by_move ta ON ta.move_id = aml.move_id
       WHERE aal.date >= %s
         AND aal.date <= %s)

SELECT analytic_account_id,
       sum(amount_untaxed) AS amount_untaxed,
       sum(amount_total)   AS amount_total
FROM detailed_result
GROUP BY analytic_account_id""", (date_from, date_to,))
        return self.env.cr.dictfetchall()

    @api.model
    def get_analytic_revenue_subtotals_for_period(self, date_from, date_to, separated=True):
        analytic_subtotals_for_period = self.get_analytic_subtotals_for_period(date_from, date_to)
        revenue_dict = {}
        cost_of_sales_dict = {}
        net_overhead_costs_dict = {}
        for item in analytic_subtotals_for_period:
            analytic_account = self.env['account.analytic.account'].browse(item['analytic_account_id'])
            amount_total_for_period = item['amount_total']
            if analytic_account.bilan_type == 'revenue':
                revenue_dict[analytic_account] = amount_total_for_period
            if analytic_account.bilan_type == 'cost_of_sales':
                cost_of_sales_dict[analytic_account] = amount_total_for_period
            if analytic_account.bilan_type == 'net_overhead_costs':
                net_overhead_costs_dict[analytic_account] = amount_total_for_period
        if separated:
            return revenue_dict, cost_of_sales_dict, net_overhead_costs_dict
        else:
            expense_dict = {}
            for account in cost_of_sales_dict:
                expense_dict[account] = expense_dict.get(account, 0) + cost_of_sales_dict[account]
            for account in net_overhead_costs_dict:
                expense_dict[account] = expense_dict.get(account, 0) + net_overhead_costs_dict[account]
            return revenue_dict, expense_dict

    @api.model
    def get_tax_amount_between_dates(self, date_from, date_to):
        tva_reelle = 0
        tva_reconciliee = 0
        for subtotal_data in self.get_analytic_subtotals_for_period(date_from, date_to):
            if subtotal_data['amount_total'] >= 0:
                tva_reelle += (subtotal_data['amount_total'] - subtotal_data['amount_untaxed'])
                continue
            tva_reconciliee += (subtotal_data['amount_untaxed'] - subtotal_data['amount_total'])
        return tva_reelle, tva_reconciliee

    @api.multi
    def get_months_list(self):
        result = []
        date_from_first_month_day = fields.Date.from_string("%s-%s-01" % (self.year_from, self.month_from))
        date_to_first_month_day = fields.Date.from_string("%s-%s-01" % (self.year_to, self.month_to))
        while date_from_first_month_day <= date_to_first_month_day:
            result += [(fields.Date.to_string(date_from_first_month_day),
                        fields.Date.to_string(date_from_first_month_day + relativedelta(months=1) -
                                              relativedelta(days=1)))]
            date_from_first_month_day += relativedelta(months=1)
        return result
