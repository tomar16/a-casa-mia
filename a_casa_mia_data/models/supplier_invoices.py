# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import base64
import logging
import os

import unidecode
import xlrd

from odoo import models, api, exceptions

_logger = logging.getLogger(__name__)


class AcmImportSupplierInvoices(models.Model):
    _inherit = 'customer.file.to.import'

    @api.multi
    def generate_out_csv_files(self):
        self.ensure_one()
        super(AcmImportSupplierInvoices, self).generate_out_csv_files()
        if self != self.env.ref('a_casa_mia_data.file_demo_supplier_invoices'):
            return

        partner_dict_result = {}
        invoice_dict_result = {}
        invoice_line_dict_result = {}

        purchase_account = self.env['account.account'].search([('code', '=', '607100')], limit=1)
        if not purchase_account:
            raise exceptions.UserError(u"Imposible de trouver le compte 607100")
        purchase_account_xml_id = self.get_external_id_or_create_one(purchase_account)

        workbook = xlrd.open_workbook(file_contents=base64.b64decode(self.file),
                                      encoding_override='UTF-8',
                                      logfile=open(os.devnull, 'w'))
        for sheet_index in range(workbook.nsheets):
            worksheet = workbook.sheet_by_index(sheet_index)
            for row_number in range(1, worksheet.nrows):
                account_name = worksheet.name
                line = [cell.value for cell in worksheet.row(row_number)]
                if not line or not line[0]:
                    continue
                if "TOTAL" in line:
                    continue

                first_column = line[0]
                partner_name = first_column
                if account_name == "Consomable":
                    partner_name = "Consommable"
                    account_name = "Consommable"
                if account_name == "Alcohol":
                    account_name = "Alcool"
                if account_name == "Boissons Soft & Bierres":
                    account_name = "Boissons soft & bières"
                if account_name == "Transport ":
                    account_name = "Transport"
                if account_name == "Medical":
                    account_name = "Médical"
                if account_name == "Maintenance Électrique":
                    account_name = "Maintenance Électrique et Plomberie"
                if account_name == "Consomable":
                    account_name = "Consommable"
                if account_name == "Divers":
                    account_name = self.env.ref('fill_empty_analytic_accounts.analytic_account_purchases').name
                    partner_name = "Divers"
                partner_xml_id = 'partner_%s' % (partner_name.lower().
                                                 replace("'", "").
                                                 replace("/", "_").
                                                 replace("%", "").
                                                 replace("+", "_").
                                                 replace(" ", "").
                                                 replace(".", "").
                                                 replace(",", ""))
                # Let's unaccent this string
                partner_xml_id = unidecode.unidecode(partner_xml_id)
                analytic_account = self.env['account.analytic.account'].search([('name', '=', account_name)], limit=1)
                if not analytic_account:
                    raise exceptions.UserError(u"No analytic account found for %s" % account_name)
                expense_account_xml_id = self.get_external_id_or_create_one(analytic_account)
                partner_dict_result[partner_xml_id] = {
                    'name': partner_name,
                    'supplier': 'True',
                    'customer': 'False',
                    'income_analytic_account_id/id': 'fill_empty_analytic_accounts.analytic_account_sales',
                    'expense_analytic_account_id/id': expense_account_xml_id,
                }
                index = 0
                for amount in line[2:]:
                    if not amount:
                        continue
                    amount = float(amount)
                    index += 1
                    invoice_xml_id = 'invoice_%s_%s' % (partner_xml_id, index)
                    invoice_line_xml_id = 'invoice_line_%s_%s' % (partner_xml_id, index)
                    invoice_dict_result[invoice_xml_id] = {
                        'partner_id/id': partner_xml_id,
                        'date_invoice': '2019-10-31',
                        'type': amount >= 0 and 'in_invoice' or 'in_refund'
                    }
                    invoice_line_dict_result[invoice_line_xml_id] = {
                        'invoice_id/id': invoice_xml_id,
                        'name': first_column,
                        'account_id/id': purchase_account_xml_id,
                        'account_analytic_id/id': expense_account_xml_id,
                        'quantity': 1,
                        'price_unit': abs(amount),
                        'invoice_line_tax_ids/id': 'l10n_fr.1_tva_acq_normale',
                    }

        self.save_generated_csv_file('res.partner', list(list(partner_dict_result.values())[0].keys()),
                                     partner_dict_result, sequence=1)
        self.save_generated_csv_file('account.invoice', list(list(invoice_dict_result.values())[0].keys()),
                                     invoice_dict_result, sequence=2)
        self.save_generated_csv_file('account.invoice.line', list(list(invoice_line_dict_result.values())[0].keys()),
                                     invoice_line_dict_result, sequence=2)
