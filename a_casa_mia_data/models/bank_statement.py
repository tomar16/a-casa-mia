# -*- coding: utf8 -*-
#
#    Copyright (C) 2020 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

from dateutil.relativedelta import relativedelta

from odoo import models, api, fields, exceptions

_logger = logging.getLogger(__name__)


class AcmImportSupplierInvoices(models.Model):
    _inherit = 'customer.file.to.import'

    @api.multi
    def generate_out_csv_files(self):
        self.ensure_one()
        super(AcmImportSupplierInvoices, self).generate_out_csv_files()
        if self != self.env.ref('a_casa_mia_data.file_demo_bank_statement'):
            return

        bank_journal = self.env['account.journal'].search([('name', '=', "Banque")], limit=1)
        if not bank_journal:
            raise exceptions.UserError(u"Journal 'Banque' non trouvé")
        bank_statement_xml_id = 'bank_statement_november'
        balance_start = 8500
        bank_statement_dict = {
            bank_statement_xml_id: {
                'name': u"Octobre 2019",
                'journal_id/id': self.get_external_id_or_create_one(bank_journal),
                'date': '2019-11-12',
                'balance_start': balance_start,
            }
        }
        end_statement_date = '2019-10-31'
        balance_end_real = balance_start
        bank_statement_line_dict = {}
        especes_total_amount = 0
        for invoice in self.env['account.invoice'].search([('state', 'in', ['open', 'paid']),
                                                           ('date_invoice', '>=', '2019-10-01'),
                                                           ('date_invoice', '<=', '2019-10-31')]):
            # On ne traite pas les avoirs
            if 'refund' in invoice.type:
                continue
            amount_sign = invoice.type == 'out_invoice' and 1 or -1
            signed_amount = amount_sign * invoice.amount_total
            if signed_amount < 0:
                bank_statement_line_dict['statement_line_invoice_%s' % invoice.id] = {
                    'statement_id/id': bank_statement_xml_id,
                    'date': invoice.date_invoice,
                    'name': u"VIR: règlement facture %s" % invoice.number,
                    'amount': signed_amount,
                }
                balance_end_real += signed_amount
                continue
            if invoice.payment_mode_free_string == "Especes":
                especes_total_amount += signed_amount
                continue
            payment_debit_card_reception_date = invoice.date_invoice + relativedelta(days=1)
            # Les paiements sont versés en jours ouvrés
            while payment_debit_card_reception_date.weekday() in [5, 6]:
                payment_debit_card_reception_date += relativedelta(days=1)
            payment_debit_card_reception_date_string = fields.Date.to_string(payment_debit_card_reception_date)
            if payment_debit_card_reception_date_string > end_statement_date:
                continue
            balance_end_real += signed_amount
            statement_line_xml_id = 'statement_debit_card_%s' % (payment_debit_card_reception_date_string.
                                                                 replace('-', '_'))
            if statement_line_xml_id not in bank_statement_line_dict:
                bank_statement_line_dict[statement_line_xml_id] = {
                    'statement_id/id': bank_statement_xml_id,
                    'date': payment_debit_card_reception_date,
                    'name': u"Remise CB",
                    'amount': 0,
                }
            new_amount = bank_statement_line_dict[statement_line_xml_id]['amount'] + signed_amount
            bank_statement_line_dict[statement_line_xml_id]['amount'] = new_amount
        if especes_total_amount > 0:
            bank_statement_line_dict['statement_line_remise_especes_fin_mois'] = {
                'statement_id/id': bank_statement_xml_id,
                'date': '2019-10-31',
                'name': u"Remise espèces 31/10/2019",
                'amount': especes_total_amount,
            }
            balance_end_real += especes_total_amount
        bank_statement_dict[bank_statement_xml_id]['balance_end_real'] = balance_end_real

        self.save_generated_csv_file('account.bank.statement', list(list(bank_statement_dict.values())[0].keys()),
                                     bank_statement_dict, sequence=1)
        self.save_generated_csv_file('account.bank.statement.line',
                                     list(list(bank_statement_line_dict.values())[0].keys()),
                                     bank_statement_line_dict, sequence=2)
