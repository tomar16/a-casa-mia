# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

{
    'name': 'A Casa Mia - Données',
    'version': '0.1',
    'author': 'NDP Systèmes',
    'maintainer': 'NDP Systèmes',
    'category': 'Technical Settings',
    'depends': [
        'a_casa_mia_views',
    ],
    'description': """
A Casa Mia - Données
====================
Données pour l'ERP ERP A Casa Mia.
""",
    'website': 'http://www.ndp-systemes.fr',
    'data': [
        'data/users.xml',
        'data/account_reconcile_model.xml',
        'data/analytic_accounts.xml',
        'data/acm_auto_reconciliation_rules.xml',
        'data/first_budget.xml',
        'data/acm_profit_share.xml',
        'data/customer_files_to_import.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'application': False,
}
