# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

{
    'name': 'A Casa Mia - Config',
    'version': '0.1',
    'author': 'NDP Systèmes',
    'maintainer': 'NDP Systèmes',
    'category': 'Technical Settings',
    'depends': [
        'translations_improved',
        'quick_create_ir_attachment_abstract',
        'auto_delete_old_attachments',
        'account',
        'account_menu',
        'l10n_fr',
        'db_unaccent',
        'group_operators',
        'web_responsive',
        'mass_editing',
        'currency_rate_update',
        'queue_job_cron',
        'account_invoice_required_analytic_account',
        'account_invoice_provision',
        'account_invoice_mass_duplication',
        'account_invoice_mass_duplication_provision',
        'account_budget_oca',
        'account_budget_oca_no_budgetary_position',
        'account_budget_oca_no_line_dates',
        'account_budget_oca_editable_lines',
        'account_budget_oca_only_months_years',
        'account_analytic_parent',
        'product_analytic',
        'partner_analytic',
        'customer_file_importation',
        'account_invoice_recompute_taxes',
        'account_multi_currency_enter_rates_manually',
        'account_bank_statement_import',
        'account_balance_line',
        'account_analytic_required',
    ],
    'description': """
A Casa Mia - Config
===================
Développements spécifiques pour A Casa Mia.
""",
    'website': 'http://www.ndp-systemes.fr',
    'data': [
        'data/default_config_settings.xml',
        'data/a_casa_mia_companies.xml',
        'data/account_account_types.xml',
        'data/account_accounts.xml',
        'data/account_journals.xml',
        'data/analytic_accounts.xml',
        'data/customers.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'application': False,
}
