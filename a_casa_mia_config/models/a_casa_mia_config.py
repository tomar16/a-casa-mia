# -*- coding: utf8 -*-
#
# Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import models, api


class ACasaMiaConfig(models.TransientModel):
    _name = 'a_casa_mia.config'
    _description = "Configuration A Casa Mia"

    @api.model
    def update_lang(self):
        lang_fr = self.env['res.lang'].search([('code', '=', 'fr_FR')])
        if lang_fr:
            lang_fr.write({
                'date_format': "%d/%m/%Y",
                'grouping': "[3,3,3,3]",
                'decimal_point': ",",
                'thousands_sep': " ",
                'time_format': "%H:%M:%S",
            })

    @api.model
    def update_accounts(self):
        account_bank_rawbank_pos = self.env['account.account'].search([('code', '=', '512001')], limit=1)
        account_bank_rawbank_pos.write({
            'name': u"Banque (Rawbank POS : 05101-05509404401-25 USD)",
        })

    @api.model
    def update_journals(self):
        self.env['account.journal'].search([('name', '=', u"Liquidités")]).write({'active': False})
        self.env['account.journal'].search([('name', '=', u"Journal de taxes de comptabilité de trésorerie")]). \
            write({'active': False})
        self.env['account.journal'].search([('name', '=', u"Banque")]).write({
            'name': u"Banque (Rawbank POS : 05101-05509404401-25 USD)",
            'code': u"RAW"
        })
        if not self.env['account.journal'].search([('code', '=', 'VE')]):
            self.env['account.journal'].search([('type', '=', 'sale')], limit=1).write({'code': 'VE'})
        for journal in self.env['account.journal'].with_context(active_test=True).search([]):
            if not journal.is_payment_method:
                # Les journaux auto-générés ne doivent pas pouvoir servir pour un règlement
                journal.write({'inbound_payment_method_ids': [(6, 0, [])],
                               'outbound_payment_method_ids': [(6, 0, [])]})

    @api.model
    def update_taxes(self):
        # On ne garde que les TVA collectée/déductivles à 16%
        tva_collectee = self.env.ref('l10n_fr.1_tva_normale')
        tva_deductible = self.env.ref('l10n_fr.1_tva_acq_normale')
        self.env['account.tax'].search([('id', 'not in', [tva_collectee.id,
                                                          tva_deductible.id])]).write({'active': False})
        tva_collectee.write({'name': "TVA collectée (vente) 16,0%",
                             'description': "TVA 16%",
                             'amount': 16.0})
        tva_deductible.write({'name': "TVA déductible (achat) 16,0%",
                              'description': "TVA 16%",
                              'amount': 16.0})
        tva_collectee.tax_group_id.name = "TVA 16%"
        for tag in tva_collectee.tag_ids:
            if "20" in tag.name:
                tag.name = tag.name.replace("20", "16")
        for tag in tva_deductible.tag_ids:
            if "20" in tag.name:
                tag.name = tag.name.replace("20", "16")
        # Configuration compte TVA
        self.env.ref('l10n_fr.1_pcg_445711').code = '445000'

    @api.model
    def update_currencies(self):
        currencies_to_keep = [self.env.ref('base.EUR'),
                              self.env.ref('base.USD'),
                              self.env.ref('base.CDF')]
        for currency in currencies_to_keep:
            if not currency.active:
                currency.active = True
        for currency in self.env['res.currency'].search([]):
            if currency not in currencies_to_keep:
                currency.active = False
