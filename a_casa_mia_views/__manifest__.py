# -*- coding: utf8 -*-
#
#    Copyright (C) 2019 NDP Systèmes (<http://www.ndp-systemes.fr>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

{
    'name': 'A Casa Mia - Vues',
    'version': '0.1',
    'author': 'NDP Systèmes',
    'maintainer': 'NDP Systèmes',
    'category': 'Technical Settings',
    'depends': [
        'a_casa_mia_technical_models',
    ],
    'description': """
A Casa Mia - Vues
=================
Définition des vues et de l'interface pour l'ERP ERP A Casa Mia.
""",
    'website': 'http://www.ndp-systemes.fr',
    'data': [
        'views/menus.xml',
        'views/account_analytic_account.xml',
        'views/account_bank_statement.xml',
        'views/account_history_importation.xml',
        'views/account_journal.xml',
        'views/account_invoice.xml',
        'views/account_move.xml',
        'views/account_payment.xml',
        'views/ir_attachment.xml',
        'views/product.xml',
        'views/acm_auto_reconciliation_rule.xml',
        'views/acm_bilan.xml',
        'views/acm_expense_evolution.xml',
        'views/acm_profit_share.xml',
        'views/view_account_bank_statement_import.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'application': False,
}
